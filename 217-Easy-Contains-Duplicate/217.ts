/*
 * Given an integer array nums, return true if any value appears 
 * at least twice in the array, and return
 * false if every element is distinct.
*/

// Simple native solution, sets only contain unique values, so they need to be the same length
const containsDuplicate = (nums: number[]) => new Set(nums).size !== nums.length


// more involved solution, uses a hash to check if a number appears more than once
// const containsDuplicate = (nums: number[]): boolean => {
//     const counts: Record<number, number> = {};
//     for (const num of nums) {
//     num in counts ? counts[num] += 1 : counts[num] = 1;
//         if (counts[num] > 1) return true
//     }
//     return false
// };
