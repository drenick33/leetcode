/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

const mergeTwoLists = (
  l1: ListNode | null,
  l2: ListNode | null
): ListNode | null => {
  // dummy node, where the head is the begining of the merged list
  const dummy = new ListNode();
  let tail = dummy;

  // go through both lists and add the smaller value to the merged list
  while (l1 && l2) {
    if (l1.val < l2.val) {
      tail.next = l1;
      l1 = l1.next;
      tail = tail.next;
      continue;
    }

    tail.next = l2;
    l2 = l2.next;
    tail = tail.next;
  }

  // if there are any remaining nodes in either list, add them to the merged list
  if (l1) {
    tail.next = l1;
  }

  if (l2) {
    tail.next = l2;
  }

  // return the merged list
  return dummy.next;
};

// const mergeTwoListsOg = (
//   list1: ListNode | null,
//   list2: ListNode | null
// ): ListNode | null => {
//   // let head: ListNode | null = null;

//   if (!list1 && !list2) return null;
//   if (!list2) return list1;
//   if (!list1) return list2;

//   let cur1: ListNode | null = list1;
//   let cur2: ListNode | null = list2;
//   let head: ListNode | null = null;

//   if (cur1.val < cur2.val) {
//     head = cur1;
//     cur1 = cur1.next;
//   } else {
//     head = cur2;
//     cur2 = cur2.next;
//   }

//   let cur: ListNode | null = head;

//   while (cur1 && cur2) {
//     if (cur1.val < cur2.val) {
//       cur.next = cur1;
//       cur = cur.next;
//       cur1 = cur1.next;
//       continue;
//     }

//     cur.next = cur2;
//     cur = cur.next;
//     cur2 = cur2.next;
//   }

//   while (cur1) {
//     cur.next = cur1;
//     cur = cur.next;
//     cur1 = cur1.next;
//   }

//   while (cur2) {
//     cur.next = cur2;
//     cur = cur.next;
//     cur2 = cur2.next;
//   }

//   return head;
// };
