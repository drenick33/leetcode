const groupAnagrams = (strs: string[]): string[][] => {
	// set variables
    const result: string[][] = [];
	let curIndex = 0; 
	const sortedWords = strs.map((word) => word.split('').sort().join('')); // get all words sorted, so we can check for anagrams
	const orderMap = new Map<string, number>(); // match the sorted word to the index in the result array

	for(let i = 0; i < strs.length; i++) {
		const sortedWord = sortedWords[i]
		const word = strs[i]

		const existingArrayIndex = orderMap.get(sortedWord)

		if(existingArrayIndex !== undefined) {
            // if a sorted word has already been added, add the original word to the corresponding group in the array
			result[existingArrayIndex].push(word);
			continue;
		}

        // if not found, create a new group, and mark the index, increase the index
		result.push([word])
		orderMap.set(sortedWord, curIndex);
		curIndex++;
	}


	return result;
}

