https://leetcode.com/problems/car-fleet/

Solution:

1. Combine the positions and speeds of the cars, and do a reverse sort (highest position to lowest position);
2. Iterate over all of the cars, and calculate the time it needs to get to the target. Store the time in a stack (optional, but useful)
3. If the car reaches the target sooner than the car before it, that means they'll form a fleet, and we can pop this car/time from the fleet
4. Return the length of the fleet array/stack
