const hasRepeats = (items: string[]) => {
  const numericItems = items.filter((item) => !isNaN(Number(item)));
  return numericItems.length !== new Set(numericItems).size;
};

const isValidSudoku = (board: string[][]): boolean => {
  const cols = new Map<number, string[]>();
  const rows = new Map<number, string[]>();
  const sections = new Map<number, string[]>();

  // iterate through all rows
  for (let row = 0; row < board.length; row++) {
    // iterate through all columns
    for (let col = 0; col < board.length; col++) {
      const item = board[row][col];
      if (item === ".") continue;

      // check for duplicates in the column
      const currentColumn = cols.get(col);
      if (currentColumn) {
        if (currentColumn.includes(item)) return false;
        cols.set(col, [...currentColumn, item]);
      } else {
        cols.set(col, [item]);
      }

      // check for duplicates in the row
      const currentRow = rows.get(row);
      if (currentRow) {
        if (currentRow.includes(item)) return false;
        rows.set(row, [...currentRow, item]);
      } else {
        rows.set(row, [item]);
      }

      // check for duplicates in the section
      const sectionNumber = 3 * Math.floor(row / 3) + 3;
      const currentSection = sections.get(sectionNumber);
      if (currentSection) {
        if (currentSection.includes(item)) return false;
        sections.set(sectionNumber, [...currentSection, item]);
      } else {
        sections.set(sectionNumber, [item]);
      }
    }
  }

  return true;
};

// const testBoard = [
//   ["5", "3", ".", ".", "7", ".", ".", ".", "."],
//   ["6", ".", ".", "1", "9", "5", ".", ".", "."],
//   [".", "9", "8", ".", ".", ".", ".", "6", "."],
//   ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
//   ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
//   ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
//   [".", "6", ".", ".", ".", ".", "2", "8", "."],
//   [".", ".", ".", "4", "1", "9", ".", ".", "5"],
//   [".", ".", ".", ".", "8", ".", ".", "7", "9"],
// ];

// console.log(isValidSudoku(testBoard));

// const hasRepeats = (items: string[]) => {
//   const numericItems = items.filter((item) => !isNaN(Number(item)));
//   return numericItems.length !== new Set(numericItems).size;
// };

// const hasRepeat = (nums: string[]) => nums.length !== new Set(nums).size;

// const isValidSudoku = (board: string[][]): boolean => {
//   const hasValidRows = () => {
//     const numericRows = board.map((grids) =>
//       grids.filter((cell) => !isNaN(Number(cell)))
//     );
//     for (let i = 0; i < numericRows.length; i += 1) {
//       const row = numericRows[i];
//       if (hasRepeat(row)) return false;
//     }
//     return true;
//   };

//   const hasValidColumns = () => {
//     for (let i = 0; i < 9; i++) {
//       const column = board.map((row) => row[i]);
//       const numericColumn = column.filter((cell) => !isNaN(Number(cell)));
//       if (hasRepeat(numericColumn)) return false;
//     }

//     return true;
//   };

//   // const getSections = () => {
//   //   const offset = board.length;
//   //   const flatRows = board.flat();

//   //   console.log({ flatRows });

//   //   for (let rowIndex = 1; rowIndex <= flatRows.length; rowIndex += 28) {
//   //     for (let i = 0; i < board.length; i += 3) {
//   //       const start = rowIndex;
//   //       console.log({ start });
//   //       const section = [
//   //         flatRows[start],
//   //         flatRows[start + 1],
//   //         flatRows[start + 2],
//   //         flatRows[start + offset],
//   //         flatRows[start + offset + 1],
//   //         flatRows[start + offset + 2],
//   //         flatRows[start + offset * 2],
//   //         flatRows[start + offset * 2 + 1],
//   //         flatRows[start + offset * 2 + 2],
//   //       ];
//   //       console.log({ section });
//   //     }
//   //   }

//   //   return true;
//   // };

//   // @TODO combine these loops with the other ones
//   const hasValidSections = () => {
//     const sections: string[][] = [];

//     for (let row = 0; row < board.length; row += 3) {
//       for (let col = 0; col < board.length; col += 3) {
//         const section: string[] = [];

//         for (let i = row; i < row + 3; i++) {
//           for (let j = col; j < col + 3; j++) {
//             section.push(board[i][j]);
//           }
//         }
//         sections.push(section);
//       }
//     }

//     return !sections.some((section) => {
//       const numericSection = section.filter((cell) => !isNaN(Number(cell)));
//       return hasRepeat(numericSection);
//     });
//     // console.log({ sections });
//   };

//   // const hasValidSection = () => {
//   //   for (let i = 0; i < board.length; i += 3) {
//   //     const rows = [board[i], board[i + 1], board[i + 2]];
//   //     const section: string[] = [];
//   //     for (let j = 0; j < board.length; j += 3) {
//   //       // section.push(rows[0][j], rows[0][j + 1], rows[0]);
//   //     }
//   //   }
//   // };

//   if (!hasValidRows()) return false;
//   if (!hasValidColumns()) return false;
//   if (!hasValidSections()) return false;

//   // return !hasValidRows() || !hasValidColumns() || !hasValidSections();

//   return true;
// };

// const testBoard = [
//   ["5", "3", ".", ".", "7", ".", ".", ".", "."],
//   ["6", ".", ".", "1", "9", "5", ".", ".", "."],
//   [".", "9", "8", ".", ".", ".", ".", "6", "."],
//   ["8", ".", ".", ".", "6", ".", ".", ".", "3"],
//   ["4", ".", ".", "8", ".", "3", ".", ".", "1"],
//   ["7", ".", ".", ".", "2", ".", ".", ".", "6"],
//   [".", "6", ".", ".", ".", ".", "2", "8", "."],
//   [".", ".", ".", "4", "1", "9", ".", ".", "5"],
//   [".", ".", ".", ".", "8", ".", ".", "7", "9"],
// ];

// console.log(isValidSudoku(testBoard));
