const threeSum = (nums: number[]): number[][] => {
  const result: number[][] = [];
  const sorted = nums.sort((a, b) => a - b);

  for (let i = 0; i < sorted.length; i++) {
    if (i > 0 && sorted[i] === sorted[i - 1]) {
      continue; // don't use the same value twice
    }

    // similar solution as 2sum sorted
    let left = i + 1;
    let right = sorted.length - 1;

    while (left < right) {
      const sum = sorted[i] + sorted[left] + sorted[right];

      if (sum > 0) {
        right--;
        continue;
      }
      if (sum < 0) {
        left++;
        continue;
      }

      result.push([sorted[i], sorted[left], sorted[right]]);
      left++;
      while (sorted[left] === sorted[left - 1] && left < right) {
        left++;
      }
    }
  }

  return result;
};
