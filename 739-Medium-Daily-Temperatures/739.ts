const dailyTemperatures = (temperatures: number[]): number[] => {
  const result: number[] = new Array(temperatures.length).fill(0);
  const stack: number[][] = [];

  for (let i = 0; i < temperatures.length; i++) {
    const temp = temperatures[i];

    // Pop all the elements that are less than the current element
    // and calculate the difference between the current index and the popped index
    // and store it in the result array
    while (stack.length && temp > stack[stack.length - 1][0]) {
      const [_, stackIndex] = stack.pop() as number[];
      result[stackIndex] = i - stackIndex;
    }

    stack.push([temp, i]);
  }

  return result;
};
