https://leetcode.com/problems/reverse-linked-list/

Two solutions, Iterative is in most cases better, and requires less memory;
but it's also good to know the recursive solution

Iterative Solution:

1. Initialize two pointers: prev = null and cur = head.
2. Traverse the list, updating pointers:
   Store the next node.
   Reverse the cur.next to point to prev.
   Move prev to cur and cur to the next node.
3. Return prev as the new head.

Recursive Solution:

1. Base case: If the head is null, return null.
2. Recursively reverse the rest of the list.
3. Make the next node's next point to the current node (head.next.next = head).
4. Set head.next = null and return the new head.
