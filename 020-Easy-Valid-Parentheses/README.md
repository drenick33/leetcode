https://leetcode.com/problems/valid-parentheses/

Solution:

1. Create a stack to store the brackets
2. Iterate over chars in a string
3. Add open brackets to stack
4. If we get a closing bracket, check if there are any opening brackets in the stack, and if there is, check that the brackets match
