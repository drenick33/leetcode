https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/description/

1. Create two pointers, one at the start and the other at the end
2. Use a while loop to go through the numbers until we find our answer
3. Calculate the sum until it reaches the target, if so return [left + 1, right + 1];
4. If the sum is smaller than the target, increase the left (go to a higher number);
5. Otherwise if the sum is smaller than the target, decrease the right (go to a smaller number);
