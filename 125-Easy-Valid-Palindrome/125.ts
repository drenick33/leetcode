const isPalindrome = (s: string): boolean => {
  let left = 0;
  let right = s.length - 1;

  while (left < right) {
    while (left < right && !isAlphaNumeric(s[left])) {
      left = left + 1;
    }

    while (right > left && !isAlphaNumeric(s[right])) {
      right = right - 1;
    }

    if (s[left].toLowerCase() !== s[right].toLowerCase()) {
      return false;
    }

    left = left + 1;
    right = right - 1;
  }

  return true;
};

const isAlphaNumeric = (char: string) => {
  const code = char.charCodeAt(0);

  // Check if the character is a letter (a-z, A-Z) or a digit (0-9)
  return (
    (code >= 48 && code <= 57) || // '0' - '9'
    (code >= 65 && code <= 90) || // 'A' - 'Z'
    (code >= 97 && code <= 122) // 'a' - 'z'
  );
};

// original solution
// const isPalindrome = (s: string): boolean => {
//    const letters = s.replace(/[^a-zA-Z0-9]/g, '').toLowerCase()

//    for(let i = 0; i < Math.floor(letters.length / 2); i++) {
//         if(letters[i] !== letters[letters.length - i - 1]) {
//             return false
//         }
//    }
//    return true;
// };
