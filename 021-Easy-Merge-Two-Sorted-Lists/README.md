https://leetcode.com/problems/merge-two-sorted-lists/description/

1. Create a dummy node to serve as the starting point of the merged list. Initialize tail = dummy.
2. Traverse both lists (l1 and l2), adding the smaller node to tail:

   If l1.val < l2.val, append l1 and move l1 to its next node.
   Otherwise, append l2 and move l2 to its next node.
   Move tail to the newly added node.

3. Append any remaining nodes from either list (l1 or l2).
4. Return dummy.next, the head of the merged list.
