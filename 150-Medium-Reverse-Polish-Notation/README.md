https://leetcode.com/problems/evaluate-reverse-polish-notation/description/

https://en.wikipedia.org/wiki/Reverse_Polish_notation

reverse polish notation can be solved using a stack

this problem assumes that the notation is valid, there is no division by 0, and division between two integers always truncates toward zero. Answer must always be a 32-bit integer

1. Add numbers to the stack
2. Perform an operation with the top two elements of the stack when you encounter an operant
3. Remove the top two elements, and push the result to the top
4. Return
