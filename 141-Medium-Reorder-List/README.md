https://leetcode.com/problems/reorder-list/

1. identify the 2nd half of the list
2. reverse the 2nd half of the list
3. merge the two halves
