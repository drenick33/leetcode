const pairs: Record<string, string> = {
  ")": "(",
  "]": "[",
  "}": "{",
};

const closingBrackets = [")", "]", "}"];

const isValid = (s: string): boolean => {
  const stack: string[] = [];

  for (const char of s) {
    if (closingBrackets.includes(char)) {
      const openingBracket = pairs[char];

      // stack exists and brackets match
      if (stack.length && stack[stack.length - 1] === openingBracket) {
        stack.pop();
      } else {
        // stack is either empty, or brackets don't line up
        return false;
      }
      continue;
    }
    // add opening bracket to stack
    stack.push(char);
  }
  return !stack.length; // stack is empty, all brackets are cleared
};
