https://leetcode.com/problems/longest-consecutive-sequence/

Solution:

1. Create a Set of all the numbers, so that we have no duplicates
2. Iterate over the set, look for the start of each sequence (numbers which don't have any values before them), and continuously check how many consecutive numbers are in front of it, storing the count
3. If the count is higher than the highest encountered, update the highest encountered
