https://leetcode.com/problems/group-anagrams/description/

Solution:

1. Sort all the words alphabetically, to match anagrams
2. Create a map (or object) to help match groups
3. Create an array for each group
