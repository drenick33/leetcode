interface Car {
  position: number;
  speed: number;
}

const carFleet = (target: number, positions: number[], speeds: number[]) => {
  const cars: Car[] = positions
    .map((p, index) => {
      return {
        position: p,
        speed: speeds[index],
      };
    })
    .sort((a, b) => b.position - a.position);

  const fleets: number[] = [];

  cars.forEach((car) => {
    fleets.push((target - car.position) / car.speed);
    if (
      fleets.length > 1 &&
      fleets[fleets.length - 1] <= fleets[fleets.length - 2]
    ) {
      fleets.pop();
    }
  });

  return fleets.length;
};
