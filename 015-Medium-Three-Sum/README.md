https://leetcode.com/problems/3sum/description/

1. Sort the array
2. We need 3 pointers, Iterate over the array with the first one. But skip repeated numbers
3. Pointer 2 is the left of the array, pointer 3 is the right of the array
4. Similar to 167 - Two Sum Sorted, we modify the left and right pointers until we find the desired sum
5. Additionally, we since we have multiple results, we need to increment the left pointer afterwards, and repeat
6. Return the result we calculate inside the loop
