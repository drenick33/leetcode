const operands = ["+", "-", "*", "/"];

function evalRPN(tokens: string[]): number {
  const stack: number[] = [];

  const performOperation = (operand: string) => {
    let result: number = 0;
    const operandOne = stack[stack.length - 2];
    const operandTwo = stack[stack.length - 1];
    switch (operand) {
      case "+":
        result = operandOne + operandTwo;
        break;
      case "-":
        result = operandOne - operandTwo;
        break;
      case "*":
        result = operandOne * operandTwo;
        break;
      case "/":
        result = Math.trunc(operandOne / operandTwo);
        break;
    }
    stack.splice(-2, 2);
    stack.push(result);
  };

  tokens.forEach((token) => {
    if (operands.includes(token)) {
      performOperation(token);
      return;
    }
    stack.push(Number(token));
  });

  return stack[0];
}
