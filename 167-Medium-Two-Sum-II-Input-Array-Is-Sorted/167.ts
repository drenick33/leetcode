const twoSum = (numbers: number[], target: number): number[] => {
  let left = 0; // Start pointer
  let right = numbers.length - 1; // End pointer

  while (left < right) {
    const sum = numbers[left] + numbers[right];

    if (sum === target) {
      return [left + 1, right + 1]; // 1-based index as per problem requirements
    }

    if (sum < target) {
      left++; // Increase the left pointer to get a larger sum
      continue;
    }

    right--; // Decrease the right pointer to get a smaller sum
  }

  // Unreachable since there is always exactly one solution
  return [];
};
