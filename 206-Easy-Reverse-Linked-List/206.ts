/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

// iterate method, time O(n), space O(1)
const reverseList = (head: ListNode | null): ListNode | null => {
  let prev: ListNode | null = null;
  let cur: ListNode | null = head;

  while (cur) {
    const next = cur.next;
    cur.next = prev;
    prev = cur;
    cur = next;
  }

  return prev;
};

// reccursive solution
function reverseListRecursive(head: ListNode | null): ListNode | null {
  if (!head) return null;

  let newHead: ListNode | null = head;
  if (newHead.next) {
    newHead = reverseListRecursive(head.next);
    if (head.next) head.next.next = head;
  }
  head.next = null;

  return newHead;
}
