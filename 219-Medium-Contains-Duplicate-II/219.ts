/*
* Given an integer array nums and an integer k, 
* return true if there are two distinct indices
* i and j in the array such that nums[i] == nums[j] 
* and abs(i - j) <= k.
*/

// In simple terms, find two elements that are the same
// but are no more than k elements apart

// best solution here, uses a moving window solution
// creates a set that has k elements, and moves
// it until it finds a duplicate, already in the set
// if nothing is found, returns false
const containsNearbyDuplicate = (nums: number[], k: number): boolean => {
    const set = new Set<number>();

    for (let i = 0; i < nums.length; i++) {
        if (set.has(nums[i])) return true;
        set.add(nums[i]);
        if (set.size > k) set.delete(nums[i - k]);
    }
    return false;
}

// const containsNearbyDuplicate = (nums: number[], k: number): boolean => {
//     const numIndexMap: Record<number, number> = {};

//     for (let i = 0; i < nums.length; i++) {
//         const number = nums[i];
//         if (number in numIndexMap && i - numIndexMap[number] <= k) return true;

//         numIndexMap[number] = i
//     }
//     return false
// }


// const containsNearbyDuplicate = (nums: number[], k: number): boolean  => {
//     const testedNumbers: number[] = [];

//     for(let i = 0; i < nums.length - 1; i ++) {
//         const number = nums[i]

//         for (let j = i + 1; j < nums.length && j - i <= k; j++) {
//             const number2 = nums[j];

//             if(number === number2) return true
//         }
//     }

//     return false


//     // const testedNumbers: number[] = []
//     // for (let i = 0; i < nums.length - 1; i++) {
//     //     const number = nums[i]
//     //     if (testedNumbers.includes(number)) {
//     //         continue; // we already checked that number
//     //     }
//     //      testedNumbers.push(number)
//     //     if (nums.lastIndexOf(number) === nums.indexOf(number)) {
//     //         continue; // only appears once
//     //     }

//     // for (let j = i + i; j < nums.length; j++) {
//     //     if(Math.abs(i - j) <= k) return true;
//     // }

//     // }

//     // return false
// };
