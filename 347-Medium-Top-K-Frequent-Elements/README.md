https://leetcode.com/problems/top-k-frequent-elements/description/

Solution:

1. Create hash map to store the count of numbers
2. Get the counts
3. Sort the counts by frequency (convert to array);
4. Get the first k elements
5. map the values to the array and return
