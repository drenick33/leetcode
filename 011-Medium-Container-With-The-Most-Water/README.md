https://leetcode.com/problems/container-with-most-water/

1. use two pointers, one at the begining, and one at the end
2. calculate the max area between the two points on the graph
3. Loop through all of the points, and move the pointers closer to each other. Always move the pointer who has a smaller value. Calculate and determine if the new area is the larger in each possible itteration
4. Stop the loop when the left pointer has past the right pointer. Return the final answer
5. This allows us to solve the problem in O(N) time, since we only visit each element once
