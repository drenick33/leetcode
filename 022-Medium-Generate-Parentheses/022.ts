// ensure # of open parentheses = n
// ensure # of closed parentheses = n
// when adding, ensure closedCount < openCount

// only add open parentheses if open < n
// only add a closing parenthesis if closed < open
// valid if open === closed === n

const generateParenthesis = (n: number): string[] => {
  const result: string[] = [];

  const backtrack = (
    openCount: number,
    closedCount: number,
    current: string
  ) => {
    if (openCount === n && closedCount === n) {
      result.push(current);
      return;
    }

    if (openCount < n) {
      backtrack(openCount + 1, closedCount, `${current}(`);
    }

    if (closedCount < openCount) {
      backtrack(openCount, closedCount + 1, `${current})`);
    }
  };

  backtrack(0, 0, "");
  return result;
};

// const generateParenthesisStackArray = (n: number): string[] => {
//   const result: string[] = [];
//   const stack: string[] = [];

//   const backtrack = (openCount: number, closedCount: number) => {
//     if (openCount === n && closedCount === n) {
//       result.push(stack.join(""));
//       return;
//     }

//     if (openCount < n) {
//       stack.push("(");
//       backtrack(openCount + 1, closedCount);
//       stack.pop();
//     }

//     if (closedCount < openCount && closedCount < n) {
//       stack.push(")");
//       backtrack(openCount, closedCount + 1);
//       stack.pop();
//     }
//   };

//   backtrack(0, 0);

//   return result;
// };
