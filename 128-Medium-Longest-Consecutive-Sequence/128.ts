const longestConsecutive = (nums: number[]): number => {
  const numberSet = new Set(nums);
  let highestCount = 0;

  numberSet.forEach((num) => {
    if (numberSet.has(num - 1)) return; // only look for starts of sequences

    let consecutiveCount = 1;
    const numRange: number[] = [num];
    while (numberSet.has(num + consecutiveCount)) {
      consecutiveCount += 1;
    }

    if (consecutiveCount > highestCount) {
      highestCount = consecutiveCount;
    }
  });

  return highestCount;
};
