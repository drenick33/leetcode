https://leetcode.com/problems/valid-palindrome/

1. Two Pointer problem
2. One pointer at the start of the string, the other at the end
3. Go through the alphanumeric characters of the string with the pointers, and check if they match when lowercase
4. Increment the left and decrement the right as necessary
5. Return false if anything doesn't match
6. When the left pointer is greater than the right pointer, stop and return true, since the string is a palindrome
