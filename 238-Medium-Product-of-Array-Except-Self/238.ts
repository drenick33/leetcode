// Given an integer array nums, return an array answer such that answer[i] is equal to the product of all the elements of nums except nums[i].
// The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.
// You must write an algorithm that runs in O(n) time and without using the division operation.

// With only one for loop
const productExceptSelf = (nums: number[]): number[] => {
  const result = new Array(nums.length).fill(1);
  let prefix = 1;
  let postfix = 1;
  nums.forEach((num, i) => {
    result[i] *= prefix;
    result[nums.length - 1 - i] *= postfix;
    prefix *= num;
    postfix *= nums[nums.length - 1 - i];
  });

  return result;
};

// Optimized Version, made by finding the product of the prefix (product of numbers prior to index i)
// and the postfix (product of numbers past index i)
// const productExceptSelf = (nums: number[]): number[] => {
//   const length = nums.length;
//   // if you expect there to be a decent amount of 0s, uncomment this, otherwise it'll be a tad slower
//   // const hasMultipleZeros = nums.indexOf(0) !== nums.lastIndexOf(0);
//   // if(hasMultipleZeros) return new Array(length).fill(0);
//   const result = new Array(length).fill(1);

//   let prefix = 1;
//   for (let i = 0; i < length; i++) {
//     result[i] *= prefix;
//     prefix *= nums[i];
//   }

//   let postfix = 1;
//   for (let i = length - 1; i >= 0; i--) {
//     result[i] *= postfix;
//     postfix *= nums[i];
//   }

//   return result;
// };

// naive solution, reduce all numbers, multiply by hand
// const productExceptSelf = (nums: number[]): number[] => {
//   const hasMultipleZeros = nums.indexOf(0) !== nums.lastIndexOf(0);
//   if(hasMultipleZeros) return nums.map(() => 0);

//   const result: number[] = []
//   nums.forEach((_,i,arr) => {
//     const newArr = Array.from(arr);
//     newArr.splice(i,1)

//     const nextProduct = newArr.reduce((a,b) =>  a * b, 1)

//     result.push(nextProduct)
//   })

//   return result
// }

// const getCounts = (nums: number[]) => {
//   const m = new Map<number, number>();

//   nums.forEach((n) => {
//     const cur = m.get(n);
//     if(cur) {m.set(n, cur + 1)
//     }
//     else {
//       m.set(n, 1)
//     }
//   })

//   return m;
// }

// a bit better, get counts of numbers and caluclate using those
// const productExceptSelf = (nums: number[]): number[] => {
//   const hasMultipleZeros = nums.indexOf(0) !== nums.lastIndexOf(0);
//   if(hasMultipleZeros) return nums.map(() => 0);
//   const results: number[] = []
//   const counts = getCounts(nums)

//   nums.forEach((num, i) => {
//     let nextNum = 1;
//     const cur = counts.get(num);
//     if(!cur) throw "error"
//     counts.set(num, cur - 1)

//     for (const [base, pow] of counts) {
//       nextNum = nextNum * (base ** pow)
//     }
//     results.push(nextNum)
//     counts.set(num, cur)
//   })

//   return results
// }
