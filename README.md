# Leetcode

Leetcode questions solved with typescript

# By Data Structure

## Arrays and Hash Maps

001 - Two
049 - Group Anagrams
128 - Longest Consecutive Sequence
217 - Contains Duplicate
219 - Contains Duplicate II
220 - Contains Duplicate III
238 - Product of Array Except Self
347 - Top K Frequent

## Stack

020 - Valid Parentheses
022 - Generate Parentheses
084 - Largest Rectangle In Histogram
150 - Evalute Reverse Polish Notation
155 - Min Stack
739 - Daily Temperatures
853 - Car Fleet

## Backtracking

022 - Generate Parentheses

## Two Pointers

011 - Container With The Most Water
015 - Three Sum
125 - Valid Palindrome
167 - Two Sum II Input Array Is Sorted

## Linked Lists

021 - Merge Two Sorted Lists
141 - Reorder List
206 - Reverse Linked List

# By Difficulty

## Easy

001 - Two Sums
020 - Valid Parentheses
021 - Merge Two Sorted Lists
125 - Valid Palindrome
141 - Reorder List
217 - Contains Duplicate

## Medium

011 - Container With The Most Water
015 - Three Sum
022 - Generate Parentheses
049 - Group Anagrams
128 - Longest Consecutive Sequence
150 - Evalute Reverse Polish Notation
155 - Min Stack
167 - Two Sum II Input Array Is Sorted
206 - Reverse Linked List
219 - Contains Duplicate II
238 - Product of Array Except Self
347 - Top K Frequent Elements
739 - Daily Temperatures
853 - Car Fleet

## Hard

084 - Largest Rectangle In Histogram
220 - Contains Duplicate III
