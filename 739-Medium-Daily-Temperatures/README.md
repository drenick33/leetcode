https://leetcode.com/problems/daily-temperatures/

Solution:

1. Create a result array with the same size of the temperatures array, initialized to 0;
2. Create a stack, containing the temp and index of temperatures
3. Iterate over the temperatures
4. When the current temp is greater than the top of the stack, pop the stack and add the difference between the index of the temp, and the stackIndex to the result array at the stack index
5. Return the result
