const maxArea = (height: number[]): number => {
  let left = 0;
  let right = height.length - 1;
  let result = 0;

  while (left < right) {
    const leftHeight = height[left];
    const rightHeight = height[right];
    const area = (right - left) * Math.min(leftHeight, rightHeight);
    if (area > result) {
      result = area;
    }

    // move the smaller line
    if (leftHeight < rightHeight) {
      left++;
      continue;
    }

    right--;
  }

  return result;
};
