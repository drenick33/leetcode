//Given an integer array nums and an integer k,
// return the k most frequent elements.
// You may return the answer in any order.

const topKFrequent = (nums: number[], k: number): number[] => {
  const counts = new Map<number, number>(); // hash map for number frequency

  // get the counts
  nums.forEach((num) => {
    counts.set(num, (counts.get(num) || 0) + 1);
  });

  return Array.from(counts) // [val, count]
    .sort((a, b) => b[1] - a[1]) // sort by frequency
    .slice(0, k) // get k elements in the array
    .map((v) => v[0]); // get the value
};
