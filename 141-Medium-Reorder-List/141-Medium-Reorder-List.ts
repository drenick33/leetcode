//  Definition for singly-linked list.
class ListNode {
  val: number;
  next: ListNode | null;
  constructor(val?: number, next?: ListNode | null) {
    this.val = val === undefined ? 0 : val;
    this.next = next === undefined ? null : next;
  }
}

/**
 Do not return anything, modify head in-place instead.
 */
const reorderList = (head: ListNode | null): void => {
  // steps to solve:
  // 1 - identify the 2nd half of the list
  // 2 - reverse the 2nd half of the list
  // 3 - merge the two halves

  const getSecondHalfOfList = (head: ListNode | null) => {
    let slow: ListNode = head as ListNode;
    let fast = head?.next;

    while (fast && fast.next) {
      slow = slow.next as ListNode;
      fast = fast.next.next;
    }

    let second = slow?.next;
    slow.next = null;
    return second;
  };

  const reverseList = (head: ListNode | null) => {
    let previous: ListNode | null = null;

    while (head) {
      const curNext = head.next;
      head.next = previous;
      previous = head;
      head = curNext;
    }
    return previous;
  };
  const mergeLists = (first: ListNode | null, second: ListNode | null) => {
    while (second) {
      const tempOne = first?.next ?? null;
      const tempTwo = second?.next ?? null;

      if (first) {
        first.next = second;
      }
      if (second) {
        second.next = tempOne;
      }

      first = tempOne;
      second = tempTwo;
    }
  };

  const secondHalf = getSecondHalfOfList(head);
  const second = reverseList(secondHalf);
  mergeLists(head, second);

  // // use 2 pointers to determine where the 2nd half of the list starts
  // let slow = head;
  // let fast = head.next;

  // while(fast && fast.next) {
  //     slow = slow.next;
  //     fast = fast.next.next;
  // }

  // // Reverse the 2nd half of the list
  // let second = slow.next;
  // slow.next = null
  // let previous = null;

  // while (second) {
  //     const tempNext = second.next; //
  //     second.next = previous;
  //     previous = second;
  //     second = tempNext;
  // }

  // // Merge two halves;

  // let first = head;
  // second = previous;
  // while (second) {
  //     const tempOne = first.next;
  //     const tempTwo = second.next;

  //     first.next = second;
  //     second.next = tempOne;
  //     first = tempOne;
  //     second = tempTwo;
  // }
};
