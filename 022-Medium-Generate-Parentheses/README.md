https://leetcode.com/problems/generate-parentheses/submissions/1352188061/

// ensure # of open parentheses = n
// ensure # of closed parentheses = n
// when adding, ensure closedCount < openCount

// only add open parentheses if open < n
// only add a closing parenthesis if closed < open
// valid if open === closed === n

Requires using backtracking

Determine rules, and go through each possible combination using recursion
