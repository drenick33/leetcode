//You are given an integer array nums and two integers indexDiff and valueDiff.
//
//Find a pair of indices (i, j) such that:
//    i != j,
//    abs(i - j) <= indexDiff.
//    abs(nums[i] - nums[j]) <= valueDiff, and
//
//Return true if such pair exists or false otherwise.


// Harder version of duplicate-1 (find duplicate value), and duplicate-2 (find within same index)
// This time, it requires us to find similar numbers (within a range) inbetween certain index values
// gets more difficult with very large arrays/values


 const getPossibleNumbers = (num: number, diff: number) => {
    const numbers: number[] = [];
    for(let i = num - diff; i <= num + diff; i++) {
        numbers.push(i)
    }
    return numbers
}

const handleLowValueDiff = (nums: number[], indexDiff: number, valueDiff: number): boolean => {
    const numberSet = new Set<number>();

    for(let i = 0; i < nums.length; i++) {
        const number = nums[i]
        const possibleNumbers = getPossibleNumbers(number, valueDiff);

        for (const val of possibleNumbers) {
            if(numberSet.has(val)) return true;
        }
        numberSet.add(number)
        if(numberSet.size > indexDiff) numberSet.delete(nums[i - indexDiff])
    }

    return false
}

const handleHighValueDiff = (nums: number[], indexDiff: number, valueDiff: number): boolean => {
    const numberSet = new Set<number>();

    for(let i = 0; i < nums.length; i++) {
        const number = nums[i];
        if(numberSet.has(number)) return true;

        for(const v of numberSet) {
            if (Math.abs(number - v) <= valueDiff) return true
        }

        numberSet.add(number)
        if(numberSet.size > indexDiff) numberSet.delete(nums[i - indexDiff])
    }
    return false
}


const containsNearbyAlmostDuplicate = (nums: number[], indexDiff: number, valueDiff: number): boolean => {
    if (valueDiff < 1000) return handleLowValueDiff(nums, indexDiff, valueDiff)
    return handleHighValueDiff(nums, indexDiff, valueDiff)

//     const numberSet = new Set<number>();

//     for(let i = 0; i < nums.length; i++) {
//         const number = nums[i]
//         const possibleNumbers = getPossibleNumbers(number, valueDiff);

//         for (const val of possibleNumbers) {
//             if(numberSet.has(val)) return true;
//         }
//         numberSet.add(number)
//         if(numberSet.size > indexDiff) numberSet.delete(nums[i - indexDiff])
//     }

//   return false

    // const numberSet = new Set<number>();

    // for(let i = 0; i < nums.length; i++) {
    //     const number = nums[i];
    //     if(numberSet.has(number)) return true;

    //     for(const v of numberSet) {
    //         if (Math.abs(number - v) <= valueDiff) return true
    //     }

    //     numberSet.add(number)
    //     if(numberSet.size > indexDiff) numberSet.delete(nums[i - indexDiff])
    // }
    // return false


    // const curNumbers: number[] = []

    // for(let i = 0; i < nums.length; i++) {
    //     const number = nums[i]
    //     if(curNumbers.includes(number)) return true // exact match

    //     if(valueDiff > 0 && curNumbers.some((n) => Math.abs(number - n) <= valueDiff)) return true // similar number in range

    //     curNumbers.push(number);
    //     if(curNumbers.length > indexDiff) curNumbers.shift()
    // }
    // return false
};
