1. store maxArea in a variable, to update as we continue
2. Use a stack to store the index and height of each rectangle
3. Iterate over heights, and add to stack;
4. If we encounter a bar with a lower height, recursively pop the item, and calculate what the max area for that rectangle is, using the height and width (calculated by difference of indexes)
5. When finished popping, update the starting index of the new rect; if possible
6. After iterrating over all numbers, go through the remaining items in the stack, and determine if one of those is the new longest rectangle.
7. Return the largest rectangle we computed
