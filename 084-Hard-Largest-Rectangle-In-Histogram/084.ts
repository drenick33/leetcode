interface BarStackItem {
  index: number;
  height: number;
}

const largestRectangleArea = (heights: number[]): number => {
  let maxArea = 0;
  // stack to store the heights, and starting index of rectangles
  const stack: BarStackItem[] = [];

  // iterate over all the numbers
  for (let i = 0; i < heights.length; i++) {
    let startIndex = i;
    const currentHeight = heights[i];

    // if the current height is smaller than what we have in the top of the stack
    // that marks the end of a rectangle, so we pop it, and calculate the new
    // max area based on the size of those rectangles. We then go back, and make
    // the start of the new rectangle, the index of the bar which is at least
    // the height of the current bar
    while (stack.length && stack[stack.length - 1].height > currentHeight) {
      const { height: barHeight, index: barIndex } =
        stack.pop() as BarStackItem;
      maxArea = Math.max(maxArea, barHeight * (i - barIndex));
      startIndex = barIndex;
    }
    stack.push({ index: startIndex, height: currentHeight });
  }

  stack.forEach(({ index, height }) => {
    maxArea = Math.max(maxArea, height * (heights.length - index));
  });

  return maxArea;
};

// First attempt, a bit brute force, and while correct
// resulted in some timeouts 92/99 testcases passed

// a Rect can only be as high as its smallest bar
// for now, let's only group forward

// const largestRectangleArea = (heights: number[]): number => {
//     const areas: number[] = []

//     // if the loop hits this, max area from there on will be: `minHeight * (index - heights.length - 1)
//     // might be able to optimise this later, to prevent needless calculations?
//     const minHeight: number = Math.min(...heights);

//     // let's loop through all of the numbers, and find the highest area it can contain;
//     // a bit brute force?

//     if(heights.length === 1) {
//         return heights[0]
//     }

//     for(let index = 0; index < heights.length; index++) {
//         const barHeight: number = heights[index];

//          // keep track of the shortest bar in the group
//         let rectHeight: number = barHeight;

//         if(rectHeight === minHeight) {
//             areas.push(rectHeight * (heights.length - index));
//             continue;
//         }

//         const areasForBar: number[] = [rectHeight]

//         for(let i = index + 1; i < heights.length; i++) {
//             const nextHeight = heights[i];

//             if(nextHeight < rectHeight) {
//                 rectHeight = nextHeight;
//             }

//             if(nextHeight === minHeight) {
//                 areasForBar.push(rectHeight * (heights.length - index));
//                 break;
//             }

//             areasForBar.push(rectHeight * (i + 1 - index))
//         }

//         areas.push(Math.max(...areasForBar))
//     }

//     return Math.max(...areas)
// };
