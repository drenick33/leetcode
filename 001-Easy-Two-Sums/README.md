https://leetcode.com/problems/product-of-array-except-self/description/

Hashmap to store the previous numbers and the index they are located at.
get the value at index i, and figure out the number, such that
nums[i] + val === target. If that number exists in our Map,
return the index of that value, and the current index.
